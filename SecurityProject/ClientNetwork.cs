﻿/*+----------------------------------------------------------------------
 ||
 ||  Class [ClientNetwork] 
 ||
 ||         Author:  [Ahmed Adel]
 ||
 ||        Purpose:  [This class is to handle cleint network operations]
 ||
 ||
 |+-----------------------------------------------------------------------
 ||
 ||   Constructors:  [the default constructor.]
 ||
 ||  Class Methods:  [sendText, Connect]
 ||
 ||
 ||
 ++-----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecurityProject
{
    class ClientNetwork
    {
        static StreamReader sr;
        static BinaryWriter bw;
        static TcpClient client;
        
        static IPEndPoint remoteEP;

        static Encryption encryptor;
        static string publicKey;
        public ClientNetwork()
        {
            encryptor = new Encryption();

        }

        /*---------------------------------------------------------------------
       |  Method sendText
       |
       |  Purpose:  sends text to the server from the client.
       |
       |  Pre-condition: the message is prepared to be sent to the server .
       |
       |  Post-condition: Sending the message to the server.
       |
       |  Parameters:
       |	text   -- string
       |
       |  Returns:  void
       *-------------------------------------------------------------------*/
        public static void sendText(string text)
        {
            encryptor.publicKey = publicKey;
            byte[]data = Encoding.Default.GetBytes(text);
            encryptor.Encrypt(data);
            bw.Write(0);
            bw.Write(encryptor.ckey.Length);
            bw.Write(encryptor.ckey);
            bw.Write(encryptor.civ.Length);
            bw.Write(encryptor.civ);
            bw.Write(encryptor.cipher.Length);
            bw.Write(encryptor.cipher);
            //bw.Flush();
            bw.Flush();

           // StreamWriter s = new StreamWriter();
          //  int bytesSent = client.Send(l.ToArray());


        }

        public static void sendImage(System.Drawing.Image image)
        {
            Bitmap bitMap = new Bitmap(image, 300, 300);
            byte[] byteImage;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                bitMap.Save(memoryStream, ImageFormat.Jpeg);
                byteImage = memoryStream.ToArray();
            }
            


            encryptor.publicKey = publicKey;
           // encryptor.Encrypt(text);
            encryptor.Encrypt(byteImage);
            bw.Write(1);
            bw.Write(encryptor.ckey.Length);
            bw.Write(encryptor.ckey);
            bw.Write(encryptor.civ.Length);
            bw.Write(encryptor.civ);
            bw.Write(encryptor.cipher.Length);
            bw.Write(encryptor.cipher);
            //bw.Flush();
            bw.Flush();
            //int i = client.Send(msg);


        }


        /*---------------------------------------------------------------------
    |  Method connect
    |
    |  Purpose:  connecting the client to the server.
    |
    |  Pre-condition: the client is not connected to the server .
    |
    |  Post-condition: the client is connected to the server.
    |
    |  Parameters:
    |	ipAddress   -- string
    |    portNum     -- int
    |
    |  Returns:  void
    *-------------------------------------------------------------------*/
        public static void connect(string ipAddress, int portNum)
        {
            try
            {
                client = new TcpClient();
                client.Connect(new IPEndPoint(IPAddress.Parse(ipAddress), portNum));
                sr = new StreamReader(client.GetStream());
                bw = new BinaryWriter(client.GetStream());

                publicKey = sr.ReadLine();
                try
                {
                   

                    // string pub = rs.ToXmlString(false);//Creates and returns an XML string containing the key of the current RSA object , false mean only the public key true mean the public and private

                    encryptor.publicKey = publicKey;
                    //encryptor.importPublic(publicKey);

                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("please enter a valid ip address");
            }


        }

    }
}

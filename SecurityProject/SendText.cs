﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecurityProject
{
    public partial class SendText : Form
    {
        string filePath;
        ClientNetwork cl;
        public SendText()
        {
            InitializeComponent();
            cl = new ClientNetwork();
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            backgroundWorker2 = new BackgroundWorker();
            backgroundWorker2.DoWork += backgroundWorker2_DoWork;
            backgroundWorker2.RunWorkerAsync(0);
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            int flag = (int)e.Argument;
            if (flag == 0)
            {
                try
                {
                    
                    ClientNetwork.sendText(textTextBox.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else {
                try
                {
                    
                    ClientNetwork.sendImage(pictureBox1.Image);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
             filePath=Helper.chooseFile();
            Bitmap bit = new Bitmap(filePath);
            pictureBox1.Image = bit;
        }

        private void sendImageButton_Click(object sender, EventArgs e)
        {
            backgroundWorker2.DoWork += backgroundWorker2_DoWork;
            backgroundWorker2.RunWorkerAsync(1);
        }

        private void SendText_Load(object sender, EventArgs e)
        {

        }
    }
}

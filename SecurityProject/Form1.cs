﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecurityProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void connect_Click(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            ClientNetwork cl = new ClientNetwork();
            string serverIp = ipAdressTextBox.Text;
            string portNumString = portNumTextBox.Text;
            int portnum = 0;
            try {
                portnum = int.Parse(portNumString);
            }
            catch (Exception exc) {
                MessageBox.Show("invalid ip address");
            }
            if (serverIp != null&&portnum!=0)
                ClientNetwork.connect(serverIp, portnum);
            else
                MessageBox.Show("please enter a valid ip address");
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
           // MessageBox.Show("connected");

            SendText s = new SendText();
            s.Show();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

      
    }
}

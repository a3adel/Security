﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecurityProject
{
    class Encryption
    {
        RSACryptoServiceProvider rs;
        SymmetricAlgorithm sa;
        public string publicKey { get; set; }
        public byte[] ckey { get; set; }
        public byte[] civ { get; set; }
        public byte[] data { get; set; }
        public byte[] cipher { get; set; }
        public Encryption()
        {
            rs = new RSACryptoServiceProvider();
          
            
        }

        public void importPublic(string publicKey) {
  
            this.publicKey = publicKey;
            rs.FromXmlString(publicKey);
        }

        public void Encrypt(byte[] data)
        {
            sa = SymmetricAlgorithm.Create("AES");
            rs.FromXmlString(publicKey);
            ckey = rs.Encrypt(sa.Key, false);
            civ = rs.Encrypt(sa.IV, false);
            
            cipher= sa.CreateEncryptor().TransformFinalBlock(data, 0, data.Length);
        }


    }
}

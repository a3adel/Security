﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServerProject
{
    public partial class ReciveData : Form
    {
        public string text{get;set;}
        public Image image { get; set; }
        public ReciveData()
        {
            InitializeComponent();
        }

        private void ReciveData_Load(object sender, EventArgs e)
        {
            if (text != null)
                recievedTextBox.Text = text;
            else
                pictureBox1.Image = image;
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServerProject
{
    class Decryption
    {
        static RSACryptoServiceProvider rs = new RSACryptoServiceProvider();
        static SymmetricAlgorithm sa;
        public static byte[] ckey { get; set; }
        public static byte[] civ { get; set; }
        static byte[] data { get; set; }
        static byte[] cipher { get; set; }
        static string pub;
        public Decryption()
        {

        }
        public static string decryptText(byte[] cipher)
        {
            //rs = new RSACryptoServiceProvider();
            sa = SymmetricAlgorithm.Create("AES");
            //rs.FromXmlString(pub);
            //MessageBox.Show("recieved key byte array length " + ckey.Length + "recieved iv byte array length " + civ.Length + " recieved data byte array length " + cipher.Length);

            sa.Key = rs.Decrypt(ckey, false);//data to be decrypted ,false use PKCS#1 padding
            sa.IV = rs.Decrypt(civ, false);
            data = sa.CreateDecryptor().TransformFinalBlock(cipher, 0, cipher.Length);
            // unformal way for dr yousef dont use it in exam, i just use it as shortcut
            string msg = Encoding.Default.GetString(data);
            return msg;

        }
        public static byte[] decrypt(byte[] cipher) {
            sa = SymmetricAlgorithm.Create("AES");
            //rs.FromXmlString(pub);
            //MessageBox.Show("recieved key byte array length " + ckey.Length + "recieved iv byte array length " + civ.Length + " recieved data byte array length " + cipher.Length);

            sa.Key = rs.Decrypt(ckey, false);//data to be decrypted ,false use PKCS#1 padding
            sa.IV = rs.Decrypt(civ, false);
            data = sa.CreateDecryptor().TransformFinalBlock(cipher, 0, cipher.Length);
            return data;
        }
        public static string exportPublicKey()
        {

            // sa = SymmetricAlgorithm.Create("AES");
            pub = rs.ToXmlString(false);//Creates and returns an XML string containing the key of the current RSA object , false mean only the public key true mean the public and private

            return pub;
        }


    }
}

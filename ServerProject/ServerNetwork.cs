﻿/*+----------------------------------------------------------------------
 ||
 ||  Class [ServerNetwork] 
 ||
 ||         Author:  [Ahmed Adel]
 ||
 ||        Purpose:  [This class is to handle server network operations]
 ||
 ||
 |+-----------------------------------------------------------------------
 ||
 ||   Constructors:  [the default constructor.]
 ||
 ||  Class Methods:  [listen]
 ||
 ||
 ||
 ++-----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServerProject
{
    class ServerNetwork
    {
        byte[] ckey;
        byte[] civ;
        byte[] cipher;
        int counter;
        byte[] data;
        int flag;
        List<TcpClient> clients;
        IPEndPoint localEndPoint;
      //  string data;
        static byte[] buffer { get; set; }
        public ServerNetwork()
        {
            //listenWorker = new BackgroundWorker();
        }

        /*---------------------------------------------------------------------
       |  Method listen
       |
       |  Purpose:  sets the server to listen to connections from the client.
       |
       |  Pre-condition: the server is not yet initialized .
       |
       |  Post-condition: the server is initialized and listenning on the specified port for connection.
       |
       |  Parameters:
       |	portNum   -- int
       |
       |  Returns:  void
       *-------------------------------------------------------------------*/
        public void listen(int portNum)
        {
            try
            {
                clients = new List<TcpClient>();

                byte[] bytes = new Byte[1024];
                //IPAddress ipAddress = IPAddress.Parse(ip);
                localEndPoint = new IPEndPoint(IPAddress.Any, portNum);

                // Create a TCP/IP socket.
                TcpListener listener = new TcpListener(IPAddress.Any, portNum);
                listener.Start();
                ReciveData rec = new ReciveData();

                while (true)
                {

                    var client = listener.AcceptTcpClient();
                    clients.Add(client);
                    counter++;
                    //  Decryption decryptor = new Decryption();
                    string pub = Decryption.exportPublicKey();
                    byte[] msg = Encoding.ASCII.GetBytes(pub);
                    StreamWriter sw = new StreamWriter(client.GetStream());
                    sw.WriteLine(pub);
                    sw.Flush();
                    // accepted.Send(msg);
                    //test to be removed
                  

                    //delete tell here
                    Thread t = new Thread(() =>
                    {
                        while (true)
                        {
                            BinaryReader br = new BinaryReader(client.GetStream());
                            flag = br.ReadInt32();
                            int len = br.ReadInt32();

                            ckey = br.ReadBytes(len);
                            len = br.ReadInt32();
                            civ = br.ReadBytes(len);

                            len = br.ReadInt32();
                            if (len == 0) // signal for closing client
                            {
                                //Report(string.Format("Client No. {0} is disconnect"));
                                client.Close();
                                return;
                            }
                            cipher = br.ReadBytes(len);
                            // buffer = new byte[accepted.SendBufferSize];
                            reportProgress();
                        }
                    });
                    t.SetApartmentState(ApartmentState.STA);
                    t.Start();

                }


            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message );
            }


        }


        private void reportProgress()
        {

            
            processData();
        }

        public void processData()
        {
            Decryption d = new Decryption();
            Decryption.ckey = ckey;
            Decryption.civ = civ;
            if (flag == 0)
            {
                string msg = Decryption.decryptText(cipher);
                MessageBox.Show(msg);
            }
            else if (flag == 1)
            {
                Image img = Helper.byteArrayToImage(Decryption.decrypt(cipher));

                
                
               
                //r.image=
            }
        }
        public void showData() {
            
        }
    }
}

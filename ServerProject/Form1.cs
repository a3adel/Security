﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServerProject
{
    public partial class Form1 : Form
    {
        // public static BackgroundWorker backgroundWorker1;
        public static bool isText;

        public Form1()
        {
            InitializeComponent();
            backgroundWorker1 = new BackgroundWorker();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void listen_Click(object sender, EventArgs e)
        {
            listen.Enabled = false;
            ServerNetwork s = new ServerNetwork();
            try
            {
                int portNum = int.Parse(portNumberTextBox.Text);
                Thread thread = new Thread(() => s.listen(portNum));
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("enter valid port number");

                listen.Enabled = true;
            }
           
        }




    

      

    }
}
